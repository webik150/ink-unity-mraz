using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{

    [SerializeField] private Transform MapPointer;
    [SerializeField] private GameObject Map;
    [SerializeField] private GameObject MapParent;
    [SerializeField] private Transform U;
    [SerializeField] private Transform V;
    [SerializeField] private Transform S;
    [SerializeField] private Transform O;
    [SerializeField] private Transform C;

    private Vector3 target;
    private float time;

    // Start is called before the first frame update
    void Start()
    {
        GoTo((string)FindObjectOfType<InkController>().story.variablesState["location"], true);
    }

    // Update is called once per frame
    void Update()
    {
        if ((target - MapPointer.localPosition).sqrMagnitude > float.Epsilon)
        {
            MapPointer.localPosition = Vector3.Lerp(MapPointer.localPosition, target, Mathf.SmoothStep(0.0f, 1.0f, time));
            time += Time.deltaTime / 3f;
        }
        else
        {
            time = 0;
        }
    }
    public void SetActive(bool state)
    {
        MapParent.SetActive(state);
    }
    
    private void OnEnable()
    {
        GoTo((string)FindObjectOfType<InkController>().story.variablesState["location"], true);
    }

    public void GoTo(string loc, bool immediate = false)
    {
        switch ((loc).ToLower())
        {
            case "o":
                GoToMedbay(immediate);
                break;
            case "u":
                GoToQuarters(immediate);
                break;
            case "v":
                GoToCommand(immediate);
                break;
            case "s":
                GoToEngine(immediate);
                break;
            case "c":
                GoToCorridor(immediate);
                break;
            default:
                Debug.LogError("Invalid location");
                break;
        }
    }

    public void GoToCorridor(bool immediate = false)
    {
        if (immediate)
        {
            MapPointer.localPosition = C.localPosition;
        }
        target = C.localPosition;
    }
    public void GoToQuarters(bool immediate = false)
    {
        if (immediate)
        {
            MapPointer.localPosition = U.localPosition;
        }
        target = U.localPosition;
    }

    public void GoToEngine(bool immediate = false)
    {
        if (immediate)
        {
            MapPointer.localPosition = S.localPosition;
        }
        target = S.localPosition;
    }

    public void GoToCommand(bool immediate = false)
    {
        if (immediate)
        {
            MapPointer.localPosition = V.localPosition;
        }
        target = V.localPosition;
    }

    public void GoToMedbay(bool immediate = false)
    {
        if (immediate)
        {
            MapPointer.localPosition = O.localPosition;
        }
        target = O.localPosition;
    }
}
