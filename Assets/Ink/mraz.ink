VAR hunger = 0
VAR brainfreeze = 3
TODO: Psychosis should add halucinations.
VAR psychosis = 0
VAR map = false
VAR stage = 0

VAR triedSouth = false
VAR triedWest = false
VAR outsideVisited = false
VAR corridorVisited = false
VAR commandVisited = false
VAR medbayVisited = false
VAR engineVisited = false
VAR roomVisited = false
VAR location = "o"
VAR lookedAtPC = false
VAR hasResearch = false
VAR hasHealthRecords = false
VAR bodyFound = false
VAR shipFound = false

TODO: Co když loď najde před přečtením zprávy
# theme: dark
# author: Kryštof Ježek
//We don't see things as they are, we see things as we are.
/*Sledovat a upravovat psychologický stav, který bude měnit přídavná jména a další vlastnosti okolí (možná i měnit okolí samotné. Rimmerův psychoměsíc).
        - brainfreeze (degraduje schopnost myslet, věci nemají konkrétní tvary/jména)
        - strach (ostrost, barvitost)
        - hlad (věci se mění na jídlo, hlad má velké oči?)
        - příčetnost (nonsense/halucinace)
    Systém hráče probudil kvůli závadě. Hráč je zmatený a se snaží zjistit co ta závada je. Ve velíně zjistí, že se blíží asteroid.
*/
->  start0

=== start0 ===
# CLEAR
Kde jsem?
    + [Kdo jsem?]# CLASS: action
    - Zima.
    +   + [Světlo. Teplo.]    # CLASS: action
        ->start

=== start ===
# theme: dark
{brainfreeze >= 3:
Postel. Deka. Teplo. Bzučení.
{hunger>=30:<> Hlad.}
  - else:
    {brainfreeze >= 2:
        Klepu se zimou v posteli v zasněžené místnosti. Jsem tak unavený.
{hunger>=10:<> Začínám mít hlad.}
    - else:
        {brainfreeze >= 1:
            Jsem na lůžku na chladné bílé ošetřovně. Pomalu rozmrzám po probuzení z umělého spánku.
        - else:
            Jsem na nemocniční posteli na ošetřovně. Zrovna jsem podstoupil probuzení ze stavu umělého spánku. Automatický detektor hrozeb musel něco zpozorovat. Měl bych zjistit co.
        }
        {hunger>=10:
            {hunger>=20:
                {hunger>=30: //30-
                    Velké kusy jemně pocukrovaných sušenek, ze kterých je složena podlaha, přitahují můj zrak. Ze stropu visí krásný lesklý bonbón. Vypadá akorát tak zralý. Musím něco sníst. 
                        * [Utrhnu bonbón.] -> darkness # CLASS: warm
                          
                -else:  //20-29
                    V dálce slyším nějaké hřmění. {~Z roztrženého rohu přikrývky vykukují lahodně vypadající vlákna cukrové vaty.||}
                }
            -else: //10-19
                <> Začínám mít trochu hlad.
            }
        -else: //0-9
            <>
        }
    }
}

*    <>{freeze_text("Pomalu.", "Pomalu vstanu.","Pomalu se zvednu.","Opatrně vstanu z lůžka.")} # CLASS: action

        
        -> medbay
    * <>{freeze_text("Pryč!", "Musím pryč!","Vyskočím z postele.","Vyskočím z postele.")}  # CLASS: action
            {brainfreeze >= 3:
                Bolest!# CLASS: pain
            - else:
                {brainfreeze >= 2:
                    Au! Spadl jsem z postele. Pořádně mi nefungují nohy.# CLASS: pain
                - else:
                    -> medbay
                }
            }
            -> medbay
    + {TURNS_SINCE(->start)<3}<>{freeze_text("Spánek.", "Odpočinu si.","Odpočinu si.","Odpočinu si.")} # CLASS: warm

        {TURNS_SINCE(->start)>2:
            Už mi to konečně myslí. # CLASS: warm
        }
        {freezedec()}
        ~ hunger = hunger + 10
        -> start

=darkness
# CLEAR
# flash
TODO: Add a flash in post-processing.
# theme: dark

Tma. Vůbec nic nevidím. V ruce držím kulatý plod stropu, který příjemně hřeje.

Musím se najíst.
* Zakousnu se do plodu.# CLASS: action
    Plod se roztříští na tisíc ostrých kousků, které se mi zavrtají do jazyka a dásní. Doušky železitého červeného sirupu, naplňují má ústa. # CLASS: pain
    *   * Chci polknout.# CLASS: action
            Ale bolest mi to nedovolí. # CLASS: pain
    *   *   * Chci vyplivnout. # CLASS: action
                Ale hlad mi to nedovolí. # CLASS: pain
    *   *   *   * CHCI PRYČ! # CLASS: action
    
    ~ hunger = 0
    ~ brainfreeze = 3
    ~ psychosis = 1
    -> start0


=== corridor ===
{brainfreeze >= 3:
        {~Chodba.|Led.|Zima.|Tma.}  {~Chodba.|Led.|Zima.|Tma.}  {~Chodba.|Led.|Zima.|Tma.}  {hunger_flavour_corridor()}
      - else:
        {brainfreeze >= 2:
           Temné chodby z ledu. {not map:Kam teď?} Je mi zima. {hunger_flavour_corridor()} {map:K postelím? Do bezpečí? Velín? Strojovna?}
        - else:
            {brainfreeze >= 1:
                {not map: 
                Jsem v nějaké chodbě. {hunger_flavour_corridor()}
                - else: 
                Jsem v hlavní chodbě. {hunger_flavour_corridor()}
            }
            - else:
                {not corridorVisited:Chodba spojující jednotlivé místnosti tohoto komplexu. {map:Na severu je velín, na východě ošetřovna, na jihu strojovna a na západě můj pokoj.}} {hunger_flavour_corridor()}
                ~ corridorVisited = true
            }
        }
    }
{not map: 
    + <>{freeze_text("Sever.","Jdu na sever.","Vydám se na sever.","Vydám se na sever.")}  # CLASS: action
    ~ hunger = hunger + 5
    ~ mapGo("v")
    -> command_room
    + {stage==0 && not triedSouth}<>{freeze_text("Jih.","Jdu na jih.","Vydám se na jih.","Vydám se na jih.")}  # CLASS: action
        {{freeze_text("Dveře. Zaseklé.","Dveře jsou zaseklé.","Nemůžu projít. Dveře jsou zaseklé.","Pokusím se projít dveřmi, ale jsou zaseklé.")}|{freeze_text("Nemůžu.","Zamrzlé dveře.","Dveře jsou zaseklé","Do strojovny se nedostanu")}}.  # CLASS: action
        ~ triedSouth = true
        -> corridor
        
    + <>{freeze_text("Východ.","Jdu na východ.","Vydám se na východ.","Vydám se na východ.")}  # CLASS: action
    ~ hunger = hunger + 5
    ~ mapGo("o")
    -> medbay
    + {stage==0 && not triedWest}<>{freeze_text("Západ.","Jdu na západ.","Vydám se na západ.","Vydám se na západ.")}# CLASS: action
        {freeze_text("Dveře. Zaseklé.","Dveře jsou zaseklé.","Nemůžu projít. Dveře jsou zaseklé.","Pokusím se projít dveřmi, ale jsou zaseklé.")}# CLASS: action
        ~ triedWest = true
        ~ mapGo("c")
        ->corridor 
        //TODO open them up in other stages.
    + {stage==1}<>{freeze_text("Západ.","Jdu na západ.","Vydám se na západ.","Vydám se na západ.")} # CLASS: action
        {not triedWest: Dveře jsou zaseklé.} Měl bych se zkusit dostat do strojovny s pomocí toho oleje co jsem našel. Třeba tam najdu způsob jak dveře opravit. # CLASS: action
        
        ~ mapGo("c")
        ->corridor

- else:
    + <>{freeze_text("Velín.","Do velína.","Vejdu do velína.","Vejdu do velína.")}  # CLASS: action
    ~ hunger = hunger + 5
    ~ mapGo("v")
    -> command_room
    + {stage==0 && brainfreeze<2 && not triedSouth}Projdu dveřmi do strojovny. # class: action
        {{freeze_text("Dveře. Zaseklé.","Dveře jsou zaseklé.","Nemůžu projít. Dveře do strojovny jsou zaseklé.","Pokusím se projít dveřmi do strojovny, ale jsou zaseklé.")}|{freeze_text("Nemůžu.","Zamrzlé dveře.","Dveře jsou zaseklé","Do strojovny se nedostanu")}}.  # CLASS: action
        ~ triedSouth = true
        ~ mapGo("c")
        -> corridor
    + {stage==1 && brainfreeze < 2}Promažu dveře do strojovny olejem. # CLASS: action
        Snažím se s ním šetřit, ale pečlivě jej aplikuji do všech rohů, děr a pístů.  # CLASS: action
    +   + Zkusím dveře otevřít. # CLASS: action
            Zabralo to! Sláva!  # CLASS: action
    +   +   + Vejdu do strojovny.
            ~ mapGo("s")
            ->engine_room
    + {stage > 1} <>{freeze_text("Strojovna.","Do strojovny.","Vejdu do strojovny.","Vejdu do strojovny.")}
            ~ mapGo("s")
            ->engine_room
    + <>{freeze_text("Ošetřovna.","Do ošetřovny.","Půjdu do ošetřovny.","Půjdu do ošetřovny.")} # CLASS: action
    ~ hunger = hunger + 5
    ~ mapGo("o")
    -> medbay
    + {stage==0 && brainfreeze < 2 && not triedWest}Půjdu k lůžkům. # CLASS: action
        {Dveře se nechtějí otevřít.|Dveře nefungují, musím je opravit.} # CLASS: action
        ~ triedWest = true
        ~ mapGo("c")
        ->corridor
        //TODO open them up in other stages.
    + {stage==1 && brainfreeze < 2}Půjdu k lůžkům. # CLASS: action
        Měl bych se zkusit dostat do strojovny s pomocí toho oleje co jsem našel. Třeba tam najdu způsob jak dveře opravit.   # CLASS: action
        
        ~ mapGo("c")
        ->corridor
}
    
    * {brainfreeze < 2}Promluvím do interkomu. -> useless_intercom # CLASS: action
    + <>{freeze_text("Mapa.","Hm... mapa.","Kouknu se na mapu.","Podívám se do mapy.")} # CLASS: action
        {print_map("c")}
        -> corridor

=useless_intercom
   Hmm... co bych měl říct?  # CLASS: action
    * "Haló?" # CLASS: action
    * "Je tam někdo?" # CLASS: action
    * "Konečně jsem se probudil, jak jste na tom?" # CLASS: action
    -
    bzzzzzt...  # CLASS: action
    *    * ["..."] -> corridor

=== medbay ===
{brainfreeze >= 3: 
    {hunger>=30:<> Hlad.}
    Bezpečí? Teplo.
- else:
    {brainfreeze >= 2:
       Potřebuju teplo. Bezpečí.
{hunger>=10:<> Mám hlad.}
    - else:
        {brainfreeze >= 1:
            Ošetřovna{not medbayVisited: ve mně vzbuzuje pocit bezpečí}. Je tu příjemně teplo. Měl bych se zahřát. {stage==1: Měl bych se zkusit dostat do strojovny.}
        - else:
            Ošetřovna{not medbayVisited: ve mně vzbuzuje pocit bezpečí. Prudké bílé světlo vytváří příjemný kontrast temným studeným chodbám}. {stage==1: Měl bych se zkusit dostat do strojovny s pomocí toho oleje co jsem našel.}
        }
        {hunger>=10:
            {hunger>=20:
                {hunger>=30: //30-
                    Velké kusy jemně pocukrovaných sušenek, ze kterých je složena podlaha, přitahují můj zrak. Ze stropu visí krásný lesklý bonbón. Vypadá akorát tak zralý. Musím něco sníst. 
                        * [Utrhnu bonbón.] -> start.darkness
                          
                -else:  //20-29
                    V dálce slyším nějaké hřmění. Z roztrženého rohu přikrývky vykukují lahodně vypadající vlákna cukrové vaty. 
                }
            -else: //10-19
                <> Začínám mít trochu hlad.
            }
        -else: //0-9
            <>
        }
    }
}
~medbayVisited = true
+ {lookedAtPC && not hasHealthRecords && brainfreeze<=1}<>{freeze_text("","","Kouknu na lékařské záznamy.","Pročtu si lékařské záznamy.")} # class: action
+   + <>{freeze_text("","","Kouknu na svůj záznam.","Pročtu si svůj záznam.")} # class: action
        {freeze_text("","","Hmm... Moje jméno, \"fyzický věk 42, nezadaný... Trpí Al Fujiho syndromem - po probuzení často trpí halucinacemi\", měl bych si dát pozor.","")} # class: action
        {brainfreeze==0:
    ┌───────────────────────────────────────────┐ # CLASS: computer
    │ [Zdravotní záznamy - výzkumná stanice 13] │ # CLASS: computer
    │    Jméno:                  DUNCAN BAYERS  │ # CLASS: computer
    │    Stav:                        NEZADANÝ  │ # CLASS: computer
    │    Fyzický věk:                       42  │ # CLASS: computer
    │    Reálný věk:                       258  │ # CLASS: computer
    │    Poslední:                         258  │ # CLASS: computer
    │                                           │ # CLASS: computer
    │    Pozn: Trpí Al Fujiho syndromem,        │ # CLASS: computer
    │    který se projevuje halucinacemi        │ # CLASS: computer
    │    po probuzení z umělého spánku.         │ # CLASS: computer   
    │                                           │ # CLASS: computer
    │    Doporučení: Choď na procházky do       │ # CLASS: computer
    │    ostatních výzkumných stanic, ať        │ # CLASS: computer
    │    se nezblázníš.                         │ # CLASS: computer
    └───────────────────────────────────────────┘ # CLASS: computer
        }
+   + <>{freeze_text("","","Nemám čas nic číst.","Vstanu od počítače.")} # class: action
-   - -> medbay
+ {brainfreeze>0}<>{freeze_text("Rozmrazovací přístroj.","Zapnu rozmrazovací přístroj.","Lehnu si na postel a zapnu rozmrazovací přístroj.","Lehnu si na postel a zapnu rozmrazovací přístroj.")} # CLASS: action
        {brainfreeze >= 3:
            Teplo. # CLASS: warm
          - else:
            {brainfreeze >= 2:
                Teplo. # CLASS: warm
            - else:
                {brainfreeze >= 1:
                    Přístroj krásně hřeje. # CLASS: warm
                    Z čekání jsem dostal hlad. # CLASS: hunger
                }
            }
        }
        {freezedec()}
        ~ hunger = hunger + 5
    ->medbay
    + <>{freeze_text("Pryč.","Vyjdu ven.","Vyjdu ven na chodbu.","Vyjdu ven na chodbu.")}# CLASS: action
    
    ~ mapGo("c")
    -> corridor
    
    + <>{freeze_text("Mapa.","Hm... mapa.","Kouknu se na mapu.","Podívám se do mapy.")} # CLASS: action
        {print_map("o")}
        -> medbay

=== engine_room ===
{brainfreeze >= 3:
    {hunger>=10:
        {hunger>=20:
            {hunger>=30: //30-
            <>
            -else:  //20-29
            <>
            }
        -else: //10-19
            <>
        }
    -else: //0-9
        <>
    }
- else:
    {brainfreeze >= 2:
        {hunger>=10:
            {hunger>=20:
                {hunger>=30: //30-
                   <>
                -else:  //20-29
                    <>
                }
            -else: //10-19
                <>
            }
        -else: //0-9
            <>
        }
    - else:
        {brainfreeze >= 1:
            {hunger>=10:
                {hunger>=20:
                    {hunger>=30: //30-
                        <>
                    -else:  //20-29
                        <> 
                    }
                -else: //10-19
                    <>
                }
            -else: //0-9
                <>
            }
        - else:
            {hunger>=10:
                {hunger>=20:
                    {hunger>=30: //30-
                       <>
                    -else:  //20-29
                        <>
                    }
                -else: //10-19
                    <>
                }
            -else: //0-9
                <>
            }
        }
    }
}
~engineVisited = true
+ <>{freeze_text("Věci.","Rozhlédnu se.","Porozhlédnu se.","Porozhlédnu se kolem.")}  # CLASS: action
    -> things
+ <>{freeze_text("Chodba.","Půjdu na chodbu.","Půjdu na chodbu.","Vrátím se na chodbu.")}  # CLASS: action

~ mapGo("c")
-> corridor


=things
* {brainfreeze == 3 && hunger >= 20}Lízátko. Tyčka studí.  # class: action
+   + Mňam.  # class: action
        Au! Moc tvrdý. # class: pain
        ->things
+ {brainfreeze == 3 && hunger >= 10 && hunger < 20} Takový to... točí se. V puse. Mám hlad.  # class: action
    Nechci. ->things
+ {brainfreeze == 3 && hunger < 10} Takový to... točí se.  # class: action
    Nechci. ->things
* {brainfreeze == 2 && hunger >= 20}Lízátko.  # class: action
+   + Líznu si  # class: action
+   +   + Líznu si  # class: action
+   +   +   + Líznu si  # class: action
+   +   +   +   + Líznu si  # class: action
+   +   +   +   +   + Líznu si  # class: action
                        AU! # class: pain
                        ->things
+ {brainfreeze == 2 && hunger >= 10 && hunger < 20}Šroubátko.  # class: action
    Co s tím?  # class: action
    ->things
+ {brainfreeze == 2 && hunger < 10}Šroubovák.  # class: action
    Co s tím?  # class: action
    ->things
* {brainfreeze == 1 && hunger >= 20}Červené lízátko.  # class: action
+   + Líznu si  # class: action
+   +   + Líznu si  # class: action
+   +   +   + Líznu si  # class: action
+   +   +   +   + Líznu si  # class: action
+   +   +   +   +   + Líznu si  # class: action
                        AU! Můj jazyk! # class: pain
                        ->things
+ {brainfreeze == 1 && hunger >= 10 && hunger < 20}Líz- šroubovák.  # class: action
Šroubovák by se mohl hodit, ale nevím k čemu, tak ho tady radši nechám. # class: action
->things
+ {brainfreeze == 1 && hunger < 10}Šroubovák.  # class: action
     Šroubovák by se mohl hodit, ale nevím k čemu, tak ho tady radši nechám. # class: action
     ->things
* {brainfreeze == 0 && hunger >= 20}Krásné červené lízátko.  # class: action
+   + Líznu si  # class: action
+   +   + Líznu si  # class: action
+   +   +   + Líznu si  # class: action
+   +   +   +   + Líznu si  # class: action
+   +   +   +   +   + Líznu si  # class: action
                        AU! Rozedřel jsem si jazyk! # class: pain
                            ->things
+ {brainfreeze == 0 && hunger >= 10 && hunger < 20}Lízovák.  # class: action
Lízovák by se mohl hodit, ale nevím k čemu, tak ho tady radši nechám. # class: action
     ->things
+ {brainfreeze == 0 && hunger < 10}Šroubovák.  # class: action
     Šroubovák by se mohl hodit, ale nevím k čemu, tak ho tady radši nechám. # class: action
     ->things
+ {brainfreeze == 3 && hunger >= 20}Nanuk? Nechápu.  # class: action
+   + Mňam.  # class: action
        Brr! Studí. # class: cold
+   +   + Mňam.  # class: action
         Au! Tvrdý! # class: pain
-   -   -   ->things
+ {brainfreeze == 3 && hunger >= 10 && hunger < 20} Takový to... bum.  # class: action
    Co já s tím?  # class: action
    ->things
+ {brainfreeze == 3 && hunger < 10} Takový to... bum.  # class: action
    Co já s tím?  # class: action
    ->things
+ {brainfreeze == 2 && hunger >= 20} Nanuk?  # class: action
+   + Líznu si  # class: action
        Brrrr! # class: cold    
*   * Kousnu si  # class: action
        AU! # class: pain
-   -   ->things
+ {brainfreeze == 2 && hunger >= 10 && hunger < 20} Nadivo.  # class: action
    Na zombie? Snad ne.  # class: action 
    ->things
+ {brainfreeze == 2 && hunger < 10}Kladivo.
    Na zombie? Snad ne.   # class: action
    ->things
+ {brainfreeze == 1 && hunger >= 20}Velký nanuk.
    Proč má tak tlustou tyčku?
+   + Líznu si  # class: action
        Brrrr! Studí! # class: cold    
*   * Zakousnu se  # class: action
        AU! Málem jsem si vylámal zuby! # class: pain
-   -   ->things
+ {brainfreeze == 1 && hunger >= 10 && hunger < 20} Klanuk.  # class: action
    To mi k ničemu nebude.
->things
+ {brainfreeze == 1 && hunger < 10}Kladivo.  # class: action
     Myslím, že nebude k ničemu.  # class: action
     ->things
+ {brainfreeze == 0 && hunger >= 20}Velký černý nanuk.  # class: action
    Proč má tak strašně tlustou tyčku?  # class: action
+   + Líznu si  # class: action
        Brrrr! Studí! # class: cold 
        ->things
*   * Zakousnu se  # class: action
        AU! Málem jsem si vylámal zuby # class: pain
        ->things
+ {brainfreeze == 0 && hunger >= 10 && hunger < 20}Kladivo.->things# class: action
+ {brainfreeze == 0 && hunger < 10}Kladivo.# class: action
->things
     Kladivo mi asi k ničemu nebude, ale aspoň vím, kde ho najít, kdybych ho potřeboval.# class: action
->things
* {brainfreeze == 3 && hunger >= 20}Brčko.# class: action
*   * Mňam.  # class: action
        Nefunguje. # class: action
*   *   * Čudlík? # class: action
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA # class: pain
        PÁLÍPÁLÍPÁLÍPÁLÍPÁLÍPÁLÍPÁLÍPÁLÍPÁLÍPÁLÍ # class: pain
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA # class: pain
        -> END
* {brainfreeze == 3 && hunger >= 10 && hunger < 20} Brčko?# class: action
     Nevím.
->things
* {brainfreeze == 3 && hunger < 10} Takový to... pálí.# class: action
     Nevím.
->things
* {brainfreeze == 2 && hunger >= 20}Kovové brčko.# class: action
     Teplo dobré. Moje. Beru.# class: action
    ~ stage = 2
    ->things
* {brainfreeze == 2 && hunger >= 10 && hunger < 20}Brč- hořák.# class: action
     Teplo dobré. Moje. Beru.# class: action
    ~ stage = 2
    ->things
* {brainfreeze == 2 && hunger < 10}Hořák.# class: action
     Teplo dobré. Moje. Beru.# class: action
    ~ stage = 2
    ->things
* {brainfreeze == 1 && hunger >= 20} Hořčák.# class: action
    Vůbec nechápu, jak se sem dostala takováhle krásná houba. Rozhodně ji tu nemůžu takhle nechat.# class: action
    ~ stage = 2
    ->things
* {brainfreeze == 1 && hunger >= 10 && hunger < 20} Hořák.# class: action
    {shipFound: Tímhle se dostanu do své lodi.|Hořák se může hodit. Beru ho s sebou.}# class: action
        ~ stage = 2
        ->things
* {brainfreeze == 1 && hunger < 10}Hořák.# class: action
     {shipFound: Tímhle se dostanu do své lodi.|Hořák se může hodit. Beru ho s sebou.}# class: action
    ~ stage = 2
    ->things
* {brainfreeze == 0 && hunger >= 20}Hořák.# class: action
    Tímhle by šly krásně oflambovat palačinky. Beru si ho k sobě.# class: action
    ~ stage = 2
    ->things
* {brainfreeze == 0 && hunger >= 10 && hunger < 20}Hořák.# class: action
{shipFound: Super! Tímhle se dostanu do své lodi.|Ledová krajina - hořák se může hodit. Beru ho s sebou.}# class: action
~ stage = 2
->things
* {brainfreeze == 0 && hunger < 10}Hořák.  # class: action
    {shipFound: Super! Tímhle se dostanu do své lodi.|Ledová krajina - hořák se může hodit. Beru ho s sebou.}# class: action
      ~ stage = 2
        ->things
+ <>{freeze_text("Stačilo.","Mám co jsem chtěl.","Přestanu hledat.","Přestanu hledat.")}  # CLASS: action
    -> engine_room

=== command_room ===
{brainfreeze >= 3:
    Velet. Přežít. Ven? 
    {hunger>=10:
        {hunger>=20:
            {hunger>=30: //30-
                <> {~HLAD!|HlAD!!|HlaD!|HLAd!|hLAD!|Jíst!}
            -else:  //20-29
                <> Hlad!
            }
        -else: //10-19
            <> {~Hlad.|Hlad...}
        }
    -else: //0-9
        <>
    }
- else:
    {brainfreeze >= 2:
    Velín. Sem jsem chtěl. Obrazovka bliká.
        {hunger>=10:
            {hunger>=20:
                {hunger>=30: //30-
                    Venku je jídlo. Musím ven.
                -else:  //20-29
                    <> {~Mám hlad.|Chci jídlo.} Venku je jídlo.
                }
            -else: //10-19
                <> Mám trochu hlad.  Venku je jídlo.
            }
        -else: //0-9
            <>
        }
    - else:
        {brainfreeze >= 1:
        {not commandVisited:Ve velící místnosti je šero. Místnost je plná různých přístrojů a čudlíků|Velící místnost}.{stage==1: Měl bych se zkusit dostat do strojovny.} Na jednom z počítačů bliká vykřičník.
            {hunger>=10:
                {hunger>=20:
                    {hunger>=30: //30-
                        Mám tak strašný hlad. Svítivé gumové bonbóny na stolech přede mnou mě lákají. Celý svět venku vypadá jedle.
                    -else:  //20-29
                        <> Mám hlad. Musím najít jídlo. Musím ven.
                    }
                -else: //10-19
                    <> {~Začínám mít trochu hlad.|Brzy budu potřebovat jídlo.|Cítím mírné otřesy.}
                }
            -else: //0-9
                <>
            }
        - else:
         Velící místnost{not commandVisited: je spoře osvětlená. Zamrzlým výhledovým oknem sem prosakuje jen trochu slunečního světla, které sotva osvítí střed místnosti. Místnost je plná různých bzučivých přístrojů a barevných čudlíků}.{stage==1: Měl bych se zkusit dostat do strojovny s pomocí toho oleje co jsem našel.} Na jednom z počítačů bliká vykřičník.
            {hunger>=10:
                {hunger>=20:
                    {hunger>=30: //30-
                        Z barevných bonbonů a čokoládových pralinek vyskládaných na stolech kolem se mi sbíhají sliny. Z výhledu velína vidím, jak se venku snáší moučkový cukr na pláně z našlehané smetany. Musím se najíst.
                    -else:  //20-29
                        <> {~Mám hlad. Musím sehnat něco k jídlu!|Měl bych si brzo najít nějaké jídlo.} Venku by mohlo něco být.
                    }
                -else: //10-19
                    <> Začínám mít trochu hlad. Měl bych najít nějaké jídlo.
                }
            -else: //0-9
                <>
            }
        }
    }
}
~ commandVisited = true
* {lookedAtPC && not hasResearch}<>{freeze_text("Výzkum!","Zachránit výzkum!","Vezmu si zálohy výzkumu.","Vezmu si zálohy výzkumu.")} # class: action
    
    ~ hasResearch = true
    {brainfreeze >= 3:
        {hunger>=10:
            {hunger>=20:
                {hunger>=30: //30-
                    Sušenka. Hlad!
                    ~hasResearch = false
                    Mňam. # class:pain
                -else:  //20-29
                    Disk. Hlad.
                }
            -else: //10-19
                Disk. Bezpečí.
            }
        -else: //0-9
            Disk. Bezpečí.
        }
    - else:
        {brainfreeze >= 2:
            {hunger>=10:
                {hunger>=20:
                    {hunger>=30: //30-
                        Lesklá sušenka.
                        ~hasResearch = false
                        Mňam. Křupe. # class:pain
                    -else:  //20-29
                        Výzkum je sušenka.
                    }
                -else: //10-19
                    Výzkum je na disku.
                }
            -else: //0-9
                Výzkum je v bezpečí.
            }
        - else:
            {brainfreeze >= 1:
                {hunger>=10:
                    {hunger>=20:
                        {hunger>=30: //30-
                            Z automatu si objednám lesklou sušenku posypanou zálohami výzkumu. Mrzí mě, že je tak malá. Umělý spánek mě donutil hladovět přes 160 let. Tomu je konec.
                            ~hasResearch = false
                            Lahodná křupavá sušenka. # class:pain
                        -else:  //20-29
                            Z počítače si vyexportuji sušenku se zálohami výzkumu. Mrzí mě, že už ho nedokončím. Umělý spánek mi dovolil tohle jablko studovat přes 160 let. Je čas ho sníst.
                        }
                    -else: //10-19
                        Z počítače si vyexportuji disk se zálohami výzkumu. Mrzí mě, že už ho nedokončím. Umělý spánek mi dovolil tuhle planetu studovat přes 160 let a stejně to bylo málo.
                    }
                -else: //0-9
                    Z počítače si vyexportuji disk se zálohami výzkumu. Mrzí mě, že už ho nedokončím. Umělý spánek mi dovolil tuhle planetu studovat přes 160 let a stejně to bylo málo.
                }
            - else:
                {hunger>=10:
                    {hunger>=20:
                        {hunger>=30: //30-
                           Z automatu si objednám lesklou sušenku posypanou zálohami výzkumu. Mrzí mě, že je tak malá. Kryokomora mě hladověla přes 160 standardních let. \\n Tolik dnů kdy jsem mohl jíst. Tolik obědů strávených jezením lahodného jídla, které vyrostlo na stromech během mého spánku.
                           ~hasResearch = false
                            Lahodná křupavá sušenka. Proč řeže? # class:pain
                        -else:  //20-29
                            Z počítače si vyexportuji sušenku se zálohami výzkumu. Mrzí mě, že už ho nedokončím. Kryokomora mi dovolila tuhle smetanovou, pocukrovanou kouli studovat přes 160 standardních let. \\n Tolik dnů v bezjídlí. Tolik dnů strávených procházením dat, místo toho abych jedl.
                        }
                    -else: //10-19
                        Z počítače si vyexportuji disk se zálohami výzkumu. Mrzí mě, že už ho nedokončím. Kryokomora mi dovolila tuhle planetu studovat přes 160 standardních let. \\n Tolik dnů v bezvědomí. Tolik dnů strávených procházením dat, které přístroje nasbíraly během mého spánku.
                    }
                -else: //0-9
                    Z počítače si vyexportuji disk se zálohami výzkumu. Mrzí mě, že už ho nedokončím. Kryokomora mi dovolila tuhle planetu studovat přes 160 standardních let. \\n Tolik dnů v bezvědomí. Tolik dnů strávených procházením dat, které přístroje nasbíraly během mého spánku.
                }
            }
        }
    }
    -> command_room
+ <>{freeze_text("Vykřičník!","Blikající obrazovka.","Přečtu si varování na počítači.","Přečtu si varování na počítači.")} # CLASS: action
    ┌──────────────────────────────────────────┐ # CLASS: computer
    │[ORBITÁLNÍ VAROVNÝ SYSTÉM ZACHYTIL HROZBU]│ # CLASS: computer
    │  \===   Datum záchytu: 34552.42.64   ===  │ # CLASS: computer
    │  \===   Úroveň hrozby: VELMI VYSOKÁ  ===  │ # CLASS: computer
    │  \==    Doporučená akce: Evakuace     ==  │ # CLASS: computer
    │                                          │ # CLASS: computer
    │       Radary zachytily blížící se        │ # CLASS: computer
    │       asteroid. Zbývá: 4d 13h 5m.        │ # CLASS: computer
    └──────────────────────────────────────────┘ # CLASS: computer
+ + [OK...] # CLASS: action
    
    {not lookedAtPC:
    ~ lookedAtPC = true
        {shipFound:
        {freeze_text("SAKRA. PRYČ. LOĎ!","Sakra! Musím pryč! Musím odletět lodí.","Zhluboka dýchat. Klid. Loď je pořád funkční, stačí se včas dostat dovnitř.","Ale sakra. Musím najít cestu pryč. Moje vesmírná loď je určitě ještě funkční.")} # class: action
        -else:
        {freeze_text("SAKRA. PRYČ.","Sakra! Musím pryč! Musím najít loď.","Zhluboka dýchat. Klid. Někde venku je loď, se kterou jsem sem přiletěl. Ta mě zachrání","Ale sakra. Čtyři dny... To je dost času na to abych našel cestu pryč. Někde venku by měla být vesmírná loď, se kterou jsem sem před lety přiletěl.")} # class: action
        }
    
    }
    -> command_room 

+ <>{freeze_text("Ven.","Půjdu ven.","Půjdu ve skafandru ven.","Obleču si skafandr a půjdu ven.")} -> outside  # CLASS: action

+ <>{freeze_text("Chodba.","Půjdu na chodbu.","Půjdu na chodbu.","Vrátím se na chodbu.")}  # CLASS: action

~ mapGo("c")
-> corridor

 + <>{freeze_text("Mapa.","Hm... mapa.","Kouknu se na mapu.","Podívám se do mapy.")}  # CLASS: action
        {print_map("v")}
        -> command_room

=== outside ===
{brainfreeze >= 3 && hunger >= 30: -> freeze_outside}
{brainfreeze >= 3:
    Zima. Nechci zimu! # CLASS: cold
    + Zpátky.  # CLASS: action
    
    ~ mapGo("v")
    -> command_room
-else:
    + {brainfreeze < 3}<>{freeze_text("Budova.","Kouknu se na budovu.","Prohlédnu si komplex.","Prohlédnu si komplex z venku.")} # class: action
        {freeze_text("","Všude je sníh. Nevidím anténu.","Celý komplex je zavátý sněhem. Je zázrak, že to nezasypalo vchod.","Z mé výzkumné stanice vidím jenom vchod a odlesky výhledu velína. Hlavní komunikační věž je naprosto zavátá sněhem. Čtyři dny jsou na opravy moc málo. Doufám, že radary varovaly i ostatní osady.")}
        -> outside
    + {brainfreeze < 3}<>{freeze_text("Ven.","Vydám se na cestu.","Vydám se prozkoumávat pláně.","Vydám se prozkoumávat pláně.")} # class: action
        -> random
        
    + {brainfreeze < 3 && shipFound} <>{freeze_text("Loď.","K vesmírné lodi.","Zpět k vesmírné lodi.","Zpět k vesmírné lodi.")} # class: action
    -> spaceship
}

=random
{brainfreeze >= 3:
    Zima. Nechci zimu! # CLASS: cold
    + Zpátky.  # CLASS: action
    ~ mapGo("v")
    -> command_room
-else:
    {stage==0:
        {~->nothing|->nothing|->crate|->creature_small|->creature_big|->body|->spaceship}
    - else:
        {~->nothing|->nothing|->creature_small|->creature_big|->body|->spaceship}
    }
}

=nothing
{freeze_text("","","Nic jsem nenašel. Jenom zasněžené pláně. Pomalu mrznu.","Venku nenacházím nic než bílé pláně. Nic k jídlu jsem nenašel. Je tady hrozná zima.")}
{freeze_text("","Nic. Jen sníh. Zima.","Pomalu mrznu.","Je tady hrozná zima.")} # class: cold
~hunger = hunger + 10
+ <>{freeze_text("","Zpátky.","Vrátím se.","Vrátím se zpět.")} # CLASS: action
{freezeinc()}
~ mapGo("v")
-> command_room
+ <>{freeze_text("","Jdu dál.","Půjdu dál.","Budu pokračovat.")} # CLASS: action
{freezeinc()}
-> random

=crate
{brainfreeze >= 2:
    Bedna s věcma!
    Tyčinky! # class: hunger
    Olej v plechovce? Beru. # class:action
- else:
    {brainfreeze >= 1:
        Našel jsem bednu se zásobami!
        Vyhrabal jsem nějaké proteinové tyčinky. # class: hunger
        Také jsem v ní našel nádobu s olejem! # class: action 
        {triedSouth:Třeba teď povolí dveře do strojovny.}
    - else:
        Našel jsem bednu se zásobami! Kdo ji tu asi nechal?
        Většina z nich je nepoužitelná, ale vyhrabal jsem nějaké proteinové tyčinky. # class:hunger
        Také jsem v ní našel nádobu s olejem! # class: action 
        {triedSouth:Třeba teď povolí dveře do strojovny.}
    }
}

~stage = stage + 1
~hunger = hunger - 10
{hunger<0:
    ~hunger = 0
}
+ <>{freeze_text("","Zpátky.","Vrátím se.","Vrátím se zpět.")} # CLASS: action
{freezeinc()}
~ mapGo("v")
-> command_room
+ <>{freeze_text("","Jdu dál.","Půjdu dál.","Budu pokračovat.")} # CLASS: action
{freezeinc()}
-> random

=creature_small
{brainfreeze >= 2:
    Ulovil jsem {~králíka|zajíce|myš|krysu}.
    Maso jsem opekl. # class: hunger
    Oheň mě ohřál. # class: warm
- else:
    {brainfreeze >= 1:
        Podařilo se mi ulovit {~králíka|zajíce|myš|krysu}.
        Maso jsem opekl a najedl se. # class: hunger
        U ohňe jsem se ohřál. # class: warm
    - else:
        Podařilo se mi ulovit {~{~sněžného|vyzáblého|polárního|bílého|vypaseného} králíka|{~sněžného|vyzáblého|polárního|bílého|vypaseného} zajíce|{~sněžnou|vyzáblou|polární|bílou|vypasenou} myš|{~sněžnou|vyzáblou|polární|bílou|vypasenou} krysu}.
        Maso jsem opekl na ohni a najedl se. # class: hunger
        Oheň mě příjemně ohřál # class: warm
    }
}

~hunger = hunger - 15
{hunger<0:
    ~hunger = 0
}
+ <>{freeze_text("","Zpátky.","Vrátím se.","Vrátím se zpět.")} # CLASS: action

    ~ mapGo("v")
-> command_room
+ <>{freeze_text("","Jdu dál.","Půjdu dál.","Budu pokračovat.")} # CLASS: action
-> random

=creature_big
{brainfreeze >= 2:
    Ulovil jsem {~medvěda|bizona|lišku|vepře}.
    Maso jsem opekl. # class: hunger
    Oheň mě ohřál. # class: warm
- else:
    {brainfreeze >= 1:
        Podařilo se mi ulovit {~medvěda|bizona|lišku|vepře}.
        Rozdělal jsem oheň.  # CLASS: warm
        Na ohni jsem si upekl maso. # CLASS: hunger
    - else:
        Podařilo se mi ulovit {~{~sněžného|ledního|polárního|krápníkového|vypaseného} medvěda|{~sněžného|ledního|polárního|krápníkového|vypaseného} bizona|{~sněžnou|lední|polární|krápníkovou|vypasenou} lišku|{~sněžného|ledního|polárního|krápníkového|vypaseného} vepře}.
        Rozdělal jsem oheň a ohřál se.  # CLASS: warm
        Na ohni jsem si upekl maso. # CLASS: hunger
    }
}
~hunger = hunger - 30
{hunger<0:
    ~hunger = 0
}
+ <>{freeze_text("","Zpátky.","Vrátím se.","Vrátím se zpět.")} # CLASS: action

    ~ mapGo("v")
-> command_room
+ <>{freeze_text("","Jdu dál.","Půjdu dál.","Budu pokračovat.")} # CLASS: action
-> random

=body
{bodyFound:
    {freeze_text("","Tady někde je ta mrtvola. Nechci ji hledat.","V dálce jsem zahlédl mrtvé tělo. Znovu už k němu nejdu.","Znovu jsem zahlédl to mrtvé tělo. Nechci jít blíž.")}
-else:
    {brainfreeze >= 2:
        Tělo! #class: pain
        Sakra... Asi taky utíkal.
    - else:
        {brainfreeze >= 1:
            Tělo! #class: pain
            Musel to být někdo z jiné výzkumné stanice!
        - else:
            Tělo ve sněhu! #class: pain
            Musel to být někdo z jiné výzkumné stanice!
            Jak dopadli ostatní?
        }
    }
}

+ {not bodyFound}<>{freeze_text("","Kdo to byl?","Kdo to mohl být?","Zkusím zjistit kdo to byl.")} #class:action
    {freeze_text("","Jmenovka je utržená. Tvář bledá. Jdu rychle pryč.","Jmenovku má strženou, ale podle skafandru je ze stanice 16. Zkoumali, jestli tu dovezená zvířata dokáží přežít. Sami to nedokázali... Musím se vzdálit.","SAKRA! To je Mike! Poznal jsem ho před několika probuzeními na stanici 16! Musím najít cestu pryč!")}
    ~ bodyFound = true
-> body
+ <>{freeze_text("","Zpátky.","Vrátím se.","Vrátím se zpět.")} # CLASS: action
    ~ bodyFound = true
    {freezeinc()}
    ~ mapGo("v")
-> command_room
+ <>{freeze_text("","Jdu dál.","Půjdu dál.","Budu pokračovat.")} # CLASS: action
    ~ bodyFound = true
    {freezeinc()}
    -> random

=spaceship
{not shipFound:
{freeze_text("LOĎ!","Moje loď!","To je moje loď!","Našel jsem svoji vesmírnou loď!")}
- else:
{freeze_text("Loď.","Moje loď.","Vrátil jsem se k lodi.","Vrátil jsem se ke svojí lodi.")}
}
~ shipFound = true
{stage < 3:
    {stage < 2: 
        {freeze_text("Led. Všude.","Všude je led. Nemůžu dovnitř.","Je celá zamrzlá! Ve strojovně možná najdu něco co mi pomůže.","Je celá pokrytá ledem. Dveře se mi nepodaří otevřít dokud neroztaje. Ve strojovně by měl být hořák.")}
    - else:
        {freeze_text("Led! Hořák?","Musím se dostat dovnitř.","Hořák led určitě roztopí.","Hořák led určitě roztopí!")}
    }
- else:
    {freeze_text("Cesta! Bezpečí!","Cesta je volná. Můžu dovnitř.","Dveře jsou otevřené. Není na co čekat, musím se dostat pryč.","Dveře jsou otevřené. Není na co čekat, musím se dostat pryč.")}
}

{freeze_text("Zima...","Je mi zima.","Začíná mi být zima.","Začíná mi být zima.")} # CLASS: cold
+ {stage>=2} <>{freeze_text("Hoř!","Hořák.","Použiju hořák.","Ohřeju dveře lodi hořákem.")}  # CLASS: action
    ~ stage = 3
    Povedlo se! Můžu se dostat do lodi. #class: action
+    + {lookedAtPC}<>{freeze_text("Dovnitř.","Dovnitř.","Vejdu dovnitř.","Vejdu dovnitř.")} # CLASS: action
    -> inside_spaceship
+    + {not lookedAtPC}<>{freeze_text("Prázdno. Ticho. Pryč.","Uvnitř nic není.","Vevnitř není nic zajímavého. Všechny věci mám na stanici.","Uvnitř není nic zajímavého. Všechny věci mám na stanici.")} # CLASS: action
    -> spaceship
+ <>{freeze_text("","Zpátky.","Vrátím se.","Vrátím se zpět.")}  # CLASS: action
{freezeinc()}
~ mapGo("v")
-> command_room
+ <>{freeze_text("","Jdu dál.","Půjdu dál.","Budu pokračovat.")}  # CLASS: action
{freezeinc()}
-> random

= inside_spaceship
{freeze_text("Domov. Bezpečí.","Konečně v bezpečí.","Nejvyšší čas odletět. Konečně jsem v bezpečí.","Nejvyšší čas odletět. Konečně jsem v bezpečí.")}

{bodyFound:{freeze_text("Tělo.","Co ostatní?","Nemůžu přestat myslet na to tělo. Co když tam někdo přežil?","Nemůžu přestat myslet na to tělo. Co když tam někdo přežil? Nemám se vrátit?")}} # class: pain

{hasResearch:{freeze_text("Výzkum v bezpečí.","Mám aspoň svůj výzkum.","Podařilo se mi aspoň zachránit výzkum.","Zachránil jsem aspoň výzkum. Snad i s posledními zálohami z ostatních stanic.")}}

Mohl jsem udělat víc? #class: slow

->END

= freeze_outside
* Ven.  # CLASS: action
*   * Jídlo.
*   *   * Těžké dveře.
*   *   *   * Cvak.
*   *   *   *   * SKAFANDR! # CLASS: pain
*   *   *   *   *   * Zima... 
*   *   *   *   *   *   *   ...
-> END

=== TODO_KONEC ===
Gratuluji, tohle je dočasný konec. Podařilo se ti schovat v kotli a přežít přilétající asteroid ->END

== function freezeinc() ==
{brainfreeze<4:
    ~ brainfreeze = brainfreeze + 1
}

== function freezedec() ==
{brainfreeze>0:
    ~ brainfreeze = brainfreeze - 1
}

== function freeze_text(a,b,c,d) ==
{brainfreeze >= 3:
    ~return a
  - else:
    {brainfreeze >= 2:
        ~return b
    - else:
        {brainfreeze >= 1:
            ~return c
        - else:
            ~return d
        }
    }
}

== function hunger_text(a,b,c,d) ==
{hunger >= 30:
    ~return a
  - else:
    {hunger >= 20:
        ~return b
    - else:
        {hunger >= 10:
            ~return c
        - else:
            ~return d
        }
    }
}

==function mark(str)==
{str} # CLASS: mark

==function print_map(loc)==
┌─────────────────┐     # CLASS: map
│        V        │     # CLASS: map
│        ║        │    # CLASS: map
│      U═╬═O      │     # CLASS: map
│        ║        │    # CLASS: map
│        S        │     # CLASS: map
└─────────────────┘     # CLASS: map
  # CLASS: maptext
V - Velín  # CLASS: maptext
U - Ubikace # CLASS: maptext
S - Strojovna # CLASS: maptext
O - Ošetřovna # CLASS: maptext
  # CLASS: maptext

{not map: {freeze_text("Mapa.","Tady to znám.","Už se začínám rovzpomínat. Vím kde jsem.","Konečně jsem si vzpomněl kde jsem. To neustálé spaní mi zamotává hlavu.")}}
~map = true

== function hunger_flavour_corridor() ==
{brainfreeze >= 3:
    {hunger>=10:
        {hunger>=20:
            {hunger>=30: //30-
                <> {~HLAD!|hLAD!|HlAD!|HLaD!!}
            -else:  //20-29
                <> Hlad.
            }
        -else: //10-19
            <> {~Hlad.|Hlad...}
        }
    -else: //0-9
        <>
    }
- else:
    {brainfreeze >= 2:
        {hunger>=10:
            {hunger>=20:
                {hunger>=30: //30-
                    Stěny jsou jedlé. Podlaha je jedlá. Těstoviny voní.
                -else:  //20-29
                    <> {~Mám hlad.|Chci jídlo.} Chodby duní.
                }
            -else: //10-19
                <> Mám trochu hlad.
            }
        -else: //0-9
            <>
        }
    - else:
        {brainfreeze >= 1:
            {hunger>=10:
                {hunger>=20:
                    {hunger>=30: //30-
                        Stěny jsou {~slizké|mokré|vlhké} a {~teplé|měkké|tvárné|voní připáleně}. Někdo by je měl sníst než se zkazí.
                    -else:  //20-29
                        <> 
                    }
                -else: //10-19
                    <> {~Začínám mít trochu hlad.|Brzy budu potřebovat jídlo.|Cítím mírné otřesy.}
                }
            -else: //0-9
                <>
            }
        - else:
            {hunger>=10:
                {hunger>=20:
                    {hunger>=30: //30-
                        Stěny této chodby jsou {~ze sýra|z masa|z bonbónů|pokryté salámem|pokryté jemnou bílou plísní}. Musím {~teď hned|okamžitě} něco sníst, jinak {~už to nevydržím|se zblázním}. {~Ze stropu visí tlusté lékořicové pendreky.|Z vnitřností komplexu se line vůně čerstvé pizzy.} {~Komplexem se šíří strašné otřesy.| | | | }
                    -else:  //20-29
                        <> {~Mám hlad. Musím sehnat něco k jídlu!|Měl bych si brzo najít nějaké jídlo.}
                    }
                -else: //10-19
                    <> Začínám mít trochu hlad. Měl bych najít nějaké jídlo.
                }
            -else: //0-9
                <>
            }
        }
    }
}

== function hungerFreezePlaceholder() ==
{brainfreeze >= 3:
    {hunger>=10:
        {hunger>=20:
            {hunger>=30: //30-
            <>
            -else:  //20-29
            <>
            }
        -else: //10-19
            <>
        }
    -else: //0-9
        <>
    }
- else:
    {brainfreeze >= 2:
        {hunger>=10:
            {hunger>=20:
                {hunger>=30: //30-
                   <>
                -else:  //20-29
                    <>
                }
            -else: //10-19
                <>
            }
        -else: //0-9
            <>
        }
    - else:
        {brainfreeze >= 1:
            {hunger>=10:
                {hunger>=20:
                    {hunger>=30: //30-
                        <>
                    -else:  //20-29
                        <> 
                    }
                -else: //10-19
                    <>
                }
            -else: //0-9
                <>
            }
        - else:
            {hunger>=10:
                {hunger>=20:
                    {hunger>=30: //30-
                       <>
                    -else:  //20-29
                        <>
                    }
                -else: //10-19
                    <>
                }
            -else: //0-9
                <>
            }
        }
    }
}

== function mapGo(loc) ==
~ location = loc
