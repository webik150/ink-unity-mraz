using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class InkText : MonoBehaviour
{
	public bool DisableFade;
	public void Destroy()
	{
		var texts = GetComponentsInChildren<Text>();
		for (var i = 0; i < texts.Length; i++)
		{
			if(!DisableFade)
				texts[i].DOColor(new Color(0.3f,0.3f,0.3f,1.0f), 1f);
		}
	}
}
