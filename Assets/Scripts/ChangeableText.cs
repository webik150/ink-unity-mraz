using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ChangeableText : MonoBehaviour
{
	public float charDelay = 0.02f;
	public float startDelay = 0.0f;

    private float modifier = 1.0f;
    [SerializeField] TMPro.TMP_Text tmpText;
    [SerializeField] Text unityText;
    public string text
    {
        set
        {
			StartCoroutine(WriteText(value));
        }
    }

    private void Update() {
        if(Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space)){
            modifier = 0.2f;
        }else{
            modifier = 1f;
        }
    }

    public IEnumerator WriteText(string text)
    {
		if(startDelay>0f){
			yield return new WaitForSeconds(startDelay);
		}
        if (tmpText)
        {
            tmpText.text = "";
        }
        if (unityText)
        {
            unityText.text = "";
        }
        for (int i = 0; i < text.Length; i++)
        {
            if (tmpText)
            {
                tmpText.text += text[i];
            }
            if (unityText)
            {
                unityText.text += text[i];
            }
			if(text[i] == '.')
				yield return new WaitForSeconds(modifier*charDelay*4);
			else if(text[i] == '?')
				yield return new WaitForSeconds(modifier*charDelay*3);
			else if(text[i] == '!')
				yield return new WaitForSeconds(modifier*charDelay*6);
			else
				yield return new WaitForSeconds(modifier*charDelay);
        }
    }
}