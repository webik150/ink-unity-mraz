﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Ink.Runtime;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using System.Collections.Generic;
using System.Linq;
using System.Collections;


// This is a super bare bones example of how to play and display a ink story in Unity.
public class InkController : MonoBehaviour
{
    [SerializeField] private float optionsDelay = 0.5f;
    private float optionsDelayTimer = 0f;
    [SerializeField] private TextAsset inkJSONAsset;
    [SerializeField] private Transform content;
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private RectTransform optionsObject;

    [SerializeField] private MapController mapController;

    [SerializeField] private VolumeProfile postProcessing;
    [SerializeField] private float[] caLevels = new float[4];
    [SerializeField] private float[] fgLevels = new float[4];
    [SerializeField] private List<string> currentTags = new List<string>();
    [SerializeField] private List<TextClassPair> textClasses = new List<TextClassPair>();
    private ChromaticAberration caPost;
    private FilmGrain fgPost;
    private Bloom bloomPost;

    // UI Prefabs
    [SerializeField] private ChangeableText textPrefab;
    [SerializeField] private Button buttonPrefab;
    public Story story;

    public static event Action<Story> OnCreateStory;

    private void Awake()
    {
        // Remove the default message
        RemoveChildren();
        StartStory();

        if (postProcessing)
        {
            postProcessing.TryGet(out caPost);
            postProcessing.TryGet(out fgPost);
            postProcessing.TryGet(out bloomPost);
        }

        var brainfreeze = (int)story.variablesState["brainfreeze"];
        fgPost.intensity.value = (fgLevels[brainfreeze]);
        caPost.intensity.value = (caLevels[brainfreeze]);

        story.ObserveVariable("brainfreeze", (varName, newValue) =>
        {
            var brainfreeze = (int)newValue;
            fgPost.intensity.value = fgLevels[brainfreeze];
            caPost.intensity.value = caLevels[brainfreeze];
        });

        story.ObserveVariable("map", (varName, newValue) =>
        {
            mapController.SetActive((bool)newValue);
            CreateCheckpoint((int)story.variablesState["stage"]);
        });

        story.ObserveVariable("location", (varName, location) =>
        {
            mapController.GoTo(((string)location).ToLower());
        });

        story.ObserveVariable("stage", (varName, stage) =>
        {
            CreateCheckpoint((int)stage);
        });
    }

    private void CreateCheckpoint(int stage)
    {
        var checkpointFile = Application.persistentDataPath + $"/checkpoint{stage}.json";
        Debug.Log("Saved to " + checkpointFile);
        System.IO.File.WriteAllText(checkpointFile, story.state.ToJson());
    }

    private void LoadLastCheckpoint()
    {
        var checkpointFile = Application.persistentDataPath + $"/checkpoint{(int)story.variablesState["stage"]}.json";
        if (System.IO.File.Exists(checkpointFile))
        {
            try
            {
                story.state.LoadJson(System.IO.File.ReadAllText(checkpointFile));
            }
            catch (Exception e)
            {
                Debug.LogError("IO Error when loading file: " + e.StackTrace);
            }
        }
    }

    private bool CheckpointExists()
    {
        var checkpointFile = Application.persistentDataPath + $"/checkpoint{(int)story.variablesState["stage"]}.json";
        return System.IO.File.Exists(checkpointFile);
    }

    private bool SaveExists()
    {
        return System.IO.File.Exists(savePath);
    }

    private void OnApplicationQuit()
    {
        fgPost.intensity.value = 0;
        caPost.intensity.value = 0;
    }

    // Creates a new Story object with the compiled story which we can then play!
    private void StartStory()
    {
        story = new Story(inkJSONAsset.text);
        if (OnCreateStory != null) OnCreateStory(story);
        //RefreshView();
        StartCoroutine(RefreshViewSlow());

    }

    // This is the main function called every time the story changes. It does a few things:
    // Destroys all the old content and choices.
    // Continues over all the lines of text, then displays all the choices. If there are no choices, the story is finished!
    private IEnumerator RefreshViewSlow()
    {
        // Remove all the UI on screen
        RemoveChildren();

        // Read all the content until we can't continue any more
        while (story.canContinue)
        {
            // Continue gets the next line of the story
            string text = story.Continue();
            currentTags = story.currentTags;
            // This removes any white space from the text.
            text = text.Trim().Replace("\\n", "\n");
            // Display the text on screen!

            if (story.currentTags.Any(x => x.Trim().ToLower().Equals("clear")))
            {
                RemoveChildren(true);
            }

            yield return StartCoroutine(CreateContentViewSlow(text));
        }

        // Display all the choices, if there are any!
        if (story.currentChoices.Count > 0)
        {
            List<Button> buttons = new List<Button>();
            for (int i = 0; i < story.currentChoices.Count; i++)
            {
                Choice choice = story.currentChoices[i];
                Button button = CreateChoiceView(choice.text.Trim());
                // Tell the button what to do when we press it
                button.onClick.AddListener(delegate
                {
                    OnClickChoiceButton(choice);
                });
                buttons.Add(button);
            }
            foreach (var button in buttons)
            {
                button.targetGraphic.CrossFadeAlpha(1f, optionsDelay, true);
            }
        }
        // If we've read all the content and there's no choices, the story is finished!
        else
        {
            Button choice = CreateChoiceView("Konec. Spustit znovu?");
            choice.onClick.AddListener(delegate
            {
                StartStory();
            }); 
            Button choice2 = CreateChoiceView("Ukončit hru.");
            choice2.onClick.AddListener(delegate
            {
                Application.Quit();
            });
            /*if (SaveExists())
            {
                Button choice2 = CreateChoiceView("Load last save.");
                choice2.onClick.AddListener(delegate
                {
                    Load();
                });
            }
            if (CheckpointExists())
            {
                Button choice3 = CreateChoiceView("Load last checkpoint.");
                choice3.onClick.AddListener(delegate
                {
                    LoadLastCheckpoint();
                });
            }*/
        }

        Invoke(nameof(DelayedVerticalRefresh), 0.1f);
    }

    // This is the main function called every time the story changes. It does a few things:
    // Destroys all the old content and choices.
    // Continues over all the lines of text, then displays all the choices. If there are no choices, the story is finished!
    private void RefreshView()
    {
        // Remove all the UI on screen
        RemoveChildren();

        // Read all the content until we can't continue any more
        while (story.canContinue)
        {
            // Continue gets the next line of the story
            string text = story.Continue();
            currentTags = story.currentTags;
            // This removes any white space from the text.
            text = text.Trim().Replace("\\n", "\n");
            // Display the text on screen!

            if (story.currentTags.Any(x => x.Trim().ToLower().Equals("clear")))
            {
                RemoveChildren(true);
            }

            CreateContentView(text);
        }

        // Display all the choices, if there are any!
        if (story.currentChoices.Count > 0)
        {
            List<Button> buttons = new List<Button>();
            for (int i = 0; i < story.currentChoices.Count; i++)
            {
                Choice choice = story.currentChoices[i];
                Button button = CreateChoiceView(choice.text.Trim());
                // Tell the button what to do when we press it
                button.onClick.AddListener(delegate
                {
                    OnClickChoiceButton(choice);
                });
                buttons.Add(button);
            }
            foreach (var button in buttons)
            {
                button.targetGraphic.CrossFadeAlpha(1f, optionsDelay, true);
            }
        }
        // If we've read all the content and there's no choices, the story is finished!
        else
        {
            Button choice = CreateChoiceView("Konec příběhu. Spustit znovu?");
            choice.onClick.AddListener(delegate
            {
                StartStory();
            });
            /*if (SaveExists())
            {
                Button choice2 = CreateChoiceView("Load last save.");
                choice2.onClick.AddListener(delegate
                {
                    Load();
                });
            }
            if (CheckpointExists())
            {
                Button choice3 = CreateChoiceView("Load last checkpoint.");
                choice3.onClick.AddListener(delegate
                {
                    LoadLastCheckpoint();
                });
            }*/
        }

        Invoke(nameof(DelayedVerticalRefresh), 0.1f);
    }

    // When we click the choice button, tell the story to choose that choice!
    private void OnClickChoiceButton(Choice choice)
    {
        story.ChooseChoiceIndex(choice.index);
        //RefreshView();
        StartCoroutine(RefreshViewSlow());
    }

    // Creates a textbox showing the the line of text
    private IEnumerator CreateContentViewSlow(string text)
    {
        ChangeableText storyText = null;
        var str = currentTags.FirstOrDefault(s => s.ToLower().StartsWith("class:"))?.Split(':')[1].Trim();
        if (!string.IsNullOrEmpty(str) && textClasses.Any(p => p.Key.Equals(str, StringComparison.InvariantCultureIgnoreCase)))
        {
            //Debug.Log("Spawning with class: " + str);
            storyText = Instantiate(textClasses.First(p => p.Key.Equals(str, StringComparison.InvariantCultureIgnoreCase)).Value) as ChangeableText;
        }
        else
        {
            storyText = Instantiate(textPrefab) as ChangeableText;
        }
        //storyText.text = text;
        storyText.transform.SetParent(content, false);
        yield return StartCoroutine(storyText.WriteText(text));
    }

    // Creates a textbox showing the the line of text
    private void CreateContentView(string text)
    {
        ChangeableText storyText = null;
        var str = currentTags.FirstOrDefault(s => s.ToLower().StartsWith("class:"))?.Split(':')[1].Trim();
        if (!string.IsNullOrEmpty(str) && textClasses.Any(p => p.Key.Equals(str, StringComparison.InvariantCultureIgnoreCase)))
        {
            //Debug.Log("Spawning with class: " + str);
            storyText = Instantiate(textClasses.First(p => p.Key.Equals(str, StringComparison.InvariantCultureIgnoreCase)).Value) as ChangeableText;
        }
        else
        {
            storyText = Instantiate(textPrefab) as ChangeableText;
        }
        storyText.text = text;
        storyText.transform.SetParent(content, false);
    }

    // Creates a button showing the choice text
    private Button CreateChoiceView(string text)
    {
        // Creates the button from a prefab
        Button choice = Instantiate(buttonPrefab);
        choice.transform.SetParent(optionsObject, false);

        // Gets the text from the button prefab
        Text choiceText = choice.GetComponentInChildren<Text>();
        choiceText.text = text;

        // Make the button expand to fit the text
        HorizontalLayoutGroup layoutGroup = choice.GetComponent<HorizontalLayoutGroup>();
        layoutGroup.childForceExpandHeight = false;
        choice.targetGraphic.CrossFadeAlpha(0f, 0f, true);
        return choice;
    }

    // Destroys all the children of this gameobject (all the UI)
    private void RemoveChildren(bool force = false)
    {
        int childCount = content.childCount;
        for (int i = childCount - 1; i >= 0; --i)
        {
            var inkText = content.GetChild(i).GetComponent<InkText>();
            if (force)
            {
                Destroy(content.GetChild(i).gameObject);
            }
            else
            {
                if (inkText != null)
                    inkText.Destroy();
                else
                    Destroy(content.GetChild(i).gameObject);
            }
        }

        childCount = optionsObject.childCount;
        for (int i = childCount - 1; i >= 0; --i)
        {
            Destroy(optionsObject.GetChild(i).gameObject);
        }
    }

    private void DelayedVerticalRefresh()
    {
        scrollRect.verticalNormalizedPosition = 0f;
    }
    [Serializable]
    class TextClassPair
    {
        public string Key;
        public ChangeableText Value;
    }

    public string savePath { get { return Application.persistentDataPath + "/savegame.json"; } }
    public void Save()
    {
        Debug.Log("Saved to " + savePath);
        System.IO.File.WriteAllText(savePath, story.state.ToJson());
    }

    public void Load()
    {
        Debug.Log("loading");
        if (SaveExists())
        {
            Debug.Log("loading2");
            try
            {
                story.state.LoadJson(System.IO.File.ReadAllText(savePath));
            }
            catch (Exception e)
            {
                Debug.LogError("IO Error when loading file: " + e.StackTrace);
            }
        }
    }
}
