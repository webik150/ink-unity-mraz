using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class TintButtonText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public ColorBlock colors;
    public Text text;
    private Button btn;
    // Start is called before the first frame update
    void Start()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(onClick);
        text.CrossFadeColor(colors.normalColor, colors.fadeDuration, true, false);
    }

    private void onClick()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.CrossFadeColor(colors.normalColor, colors.fadeDuration, true, false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.CrossFadeColor(colors.highlightedColor, colors.fadeDuration, true, false);

    }
}
